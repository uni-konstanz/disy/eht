"""Functions not available in built-in math.

Implements functions for probability and distribution.
"""

from math import *
from operator import *

def fact (x):
    """fact(x) -> x!, the faculty of x"""
    return 1 if x <= 1 else reduce(mul, xrange(2,x+1))

def perm (n,r):
    """perm(n,r) -> permutation of r selections of n possibilities

    calculates: n! / (n-r)!
    """
    if r<=1:
        if r == 1: return n
        if r == 0: return 1
        if r <= 0: return 0
    return reduce(mul, xrange(n-r+1,n+1))

def comb (n,r):
    """comb(n,r) -> combination of r selections of n possibilities"""
    return div(perm(n,r),fact(r))

def hist (items):
    """hist([items]) -> { item : frequency }

    Calculates a histogram of the frequency of items
    in the sequence [items].
    """
    h = {}
    for i in items:
        try: h[i] += 1
        except (KeyError): h[i] = 1
    return h
    

