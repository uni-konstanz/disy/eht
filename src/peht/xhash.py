"""Methods and Classes related to hashing.

Implements hash methods, Hash function classes and hash factories,
which are collections of hash functions.

An actual hash function is implemented as a method.
This method can be passed to an HashFunction object which is
initialized with the method and an upper value.
A HashFactory takes a sequence of HashFunctions
and returns a sequence of hash values using given HashFunctions.
"""

from abstract import *
from random import randint

__all__ = ["ABCHashFactory","ABCHashFunction","HashFactory","UniversalHashFactory","GeneralPurposeHashFactory","HashFunction","UniversalHashFunction","RSHash","JSHash","PJWHash","ELFHash","BKDRHash","SDBMHash","DEKHash","BPHash","FNVHash","APHash","generalpurposehashfunctions"]


class ABCHashFactory (Abstract):
    """Abstract Hash Factory class

    Defines the interface for Hash Factories.
    A Hash Factory is a collection of Hash Functions.
    A call to the factory returns a sequence of unique
    hash values produced by the hash function collection.
    """
    def __call__ (self,obj): abstract()
    def __len__ (self): abstract()

class ABCHashFunction (Abstract):
    """Abstract Hash Function class

    Defines interface for Hash Functions.
    A call to the hash function returns a hash value
    of the object.
    """
    def __call__ (self,obj): abstract()

    
class HashFactory (ABCHashFactory):
    """Implements a Hash Factory for any user defined hash functions

    Members:
    _fns        --      a tuple of hash functions
    """
    def __init__ (self, *fns):
        """Instantiate a Hash Factory

        @fns    --      sequence of hash functions
        """
        if not all(map(lambda x: isinstance(x,ABCHashFunction),fns)):
            raise TypeError("All arguments must be children of ABCHashFunction")
        self._fns = fns

    def __repr__ (self):
        return "HashFactory%s" % (repr(self._fns))
        
    def __call__ (self,obj):
        """hash(obj) -> []"""
        return {}.fromkeys([f(obj) for f in self._fns]).keys()

    def __len__ (self):
        return len(self._fns)

class UniversalHashFactory (ABCHashFactory):
    """Hash Factory for Universal Hash Functions

    Could also be done by using a HashFactory with
    UniversalHashFunctions. But it is more efficient
    to use one lut and check all values in parallel.

    Members:
    _upper      --      upper random value, exclusive
    _lut        --      random number lookup table
    _k          --      number of hash functions
    _len        --      number of random values per function
    """
    def __init__ (self,k,upper,length=32):
        """Instantiate a Universal Hash Factory

        @k      --      number of hash functions
        @length --      number of random values / lut
        @upper  --      upper bound for random values, exclusive
        """
        self._k = k
        self._upper = upper
        self._len = length
        self._lut = [[randint(0,self._upper-1) for i in xrange(self._len)] for j in xrange(self._k)]

    def __repr__ (self):
        return "UniversalHashFactory(%s,%s,%s)" % (self._k,self._len,self._upper)

    def __call__ (self,obj):
        """hash(obj) -> []"""
        h = [0] * self._k
        for l in xrange(self._len):
            if (obj>>l) & 1:
                for k in xrange(self._k):
                    h[k] ^= self._lut[k][l]
        return {}.fromkeys(h).keys()

    def __len__ (self):
        return self._k

class GeneralPurposeHashFactory (HashFactory):
    """Implements a Hash Factory for general purpose hash functions

    The k hash functions are randomly selected from those in
    generalpurposehashfunctions.

    Members:
    HashFactory.*   --      inherited from HashFactory
    _upper          --      upper random value, exclusive
    """
    def __init__ (self,k,upper):
        """Instantiate a General Purpose Hash Factory

        @k      --      number of hash functions
        @upper  --      upper bound for hash values
        """
        gpfns = generalpurposehashfunctions
        num = len(gpfns)
        if k>=num: raise IndexError("Not enough Hash Functions")
        fns = []
        chosen = []
        index = randint(0,num-1)
        for i in xrange(k):
            while index in chosen: index = randint(0,num-1)
            fns.append(HashFunction(gpfns[index],upper))
            chosen.append(index)
        HashFactory.__init__(self,*fns)
    
class HashFunction (ABCHashFunction):
    """Hash Function class

    A hash function takes a hash method and an
    upper bound value. The method hash() returns
    a hash value in the range(0,upper) using 
    provided hash method.
    Members:
    _fn     --      the hash method
    _upper  --      upper bound for hash values (exclusive)
    """
    def __init__ (self,fn,upper):
        """Instantiate a hash function

        @fn         --      the method to calculate the hash
        @upper      --      the maximum value to be returned
        """
        if type(fn) != type(lambda x:x) and type(fn) != type(hash):
            raise TypeError("Argument fn must be a function")
        self._fn = fn
        self._upper = upper

    def __repr__ (self):
        return "HashFunction(%s,%s)" % (self._fn,self._upper)
    
    def __call__ (self,obj):
        """hash(obj) -> integer"""
        return self._fn(obj) % self._upper

class UniversalHashFunction (ABCHashFunction):
    """Implements a Universal Hash Function class

    Universal hash functions use a random table to generate
    the hash value.
    Members:
    _upper  --      upper bound for hash values (exclusive)
    _lut    --      random number lookup table
    """
    def __init__ (self,length,upper):
        """Instantiate universal hash function

        @upper      --      the upper bound for random numbers, exclusive
        @length     --      number of random values to generate
        """
        self._upper = upper
        self._lut = [randint(0,self._upper-1) for i in xrange(length)]

    def __repr__ (self):
        return "UniversalHashFunction(%s,%s)" % (self._upper,len(self._lut))

    def __call__ (self,obj):
        """hash(obj) -> integer"""
        h = 0
        for b in xrange(len(self._lut)):
            if (obj>>b) & 1: h ^= self._lut[b]
        return h

###################################
# General Purpose Hash Functions
###################################

def StrHash (key):
    obj = key
    if type(key) == type(""):
        obj = 0
        for c in key: obj += ord(c)
    obj = str(obj)
    return hash(obj)

def HexHash (key):
    key = str(key)
    obj = ""
    for c in key: obj += hex(ord(c))
    return hash(obj)

def TupleHash(key):
    key = tuple(str(key))
    return hash(key)

#**************************************************************************
#*                                                                        *
#*          General Purpose Hash Function Algorithms Library              *
#*                                                                        *
#* Author: Arash Partow - 2002                                            *
#* URL: http://www.partow.net                                             *
#* URL: http://www.partow.net/programming/hashfunctions/index.html        *
#*                                                                        *
#* Copyright notice:                                                      *
#* Free use of the General Purpose Hash Function Algorithms Library is    *
#* permitted under the guidelines and in accordance with the most current *
#* version of the Common Public License.                                  *
#* http://www.opensource.org/licenses/cpl.php                             *
#*                                                                        *
#**************************************************************************

def RSHash (key):
    key = str(key)
    a    = 378551
    b    =  63689
    hash =      0
    for i in range(len(key)):
        hash = hash * a + ord(key[i])
        a = a * b
    return hash

def JSHash (key):
    key = str(key)
    hash = 1315423911
    for i in range(len(key)):
        hash ^= ((hash << 5) + ord(key[i]) + (hash >> 2))
    return hash

def PJWHash (key):
    key = str(key)
    BitsInUnsignedInt = 4 * 8
    ThreeQuarters     = long((BitsInUnsignedInt  * 3) / 4)
    OneEighth         = long(BitsInUnsignedInt / 8)
    HighBits          = (0xFFFFFFFF) << (BitsInUnsignedInt - OneEighth)
    hash              = 0
    test              = 0
    for i in range(len(key)):
        hash = (hash << OneEighth) + ord(key[i])
        test = hash & HighBits
        if test != 0:
            hash = (( hash ^ (test >> ThreeQuarters)) & (~HighBits));
    return (hash & 0x7FFFFFFF)

def ELFHash (key):
    key = str(key)
    hash = 0
    x    = 0
    for i in range(len(key)):
        hash = (hash << 4) + ord(key[i])
        x = hash & 0xF0000000
        if x != 0: hash ^= (x >> 24)
        hash &= ~x
    return hash

def BKDRHash (key):
    key = str(key)
    seed = 131 # 31 131 1313 13131 131313 etc..
    hash = 0
    for i in range(len(key)):
        hash = (hash * seed) + ord(key[i])
    return hash

def SDBMHash (key):
    key = str(key)
    hash = 0
    for i in range(len(key)):
        hash = ord(key[i]) + (hash << 6) + (hash << 16) - hash;
    return hash

def DJBHash (key):
    key = str(key)
    hash = 5381
    for i in range(len(key)):
        hash = ((hash << 5) + hash) + ord(key[i])
    return hash

def DEKHash (key):
    key = str(key)
    hash = len(key);
    for i in range(len(key)):
        hash = ((hash << 5) ^ (hash >> 27)) ^ ord(key[i])
    return hash

def BPHash (key):
    key = str(key)
    hash = 0
    for i in range(len(key)):
        hash = hash << 7 ^ ord(key[i])
    return hash

def FNVHash (key):
    key = str(key)
    fnv_prime = 0x811C9DC5
    hash = 0
    for i in range(len(key)):
        hash *= fnv_prime
        hash ^= ord(key[i])
    return hash

def APHash (key):
    key = str(key)
    hash = 0xAAAAAAAA
    for i in range(len(key)):
        if ((i & 1) == 0):
            hash ^= ((hash <<  7) ^ ord(key[i]) * (hash >> 3))
        else:
            hash ^= (~((hash << 11) + ord(key[i]) ^ (hash >> 5)))
    return hash

generalpurposehashfunctions = [hash,RSHash,JSHash,PJWHash,ELFHash,BKDRHash,SDBMHash,DEKHash,BPHash,FNVHash,APHash,StrHash,HexHash,TupleHash]
