"""A conceptual efficient hash table implementation.

Conceptual in the sense that it doesn't fully resemble the EHT
architecture but simulates it's behaviour.

The EHT serves as a wrapper for all underlying structres
like bloom filter, tables and hash factories.

The hash factory is chosen depending on the parameters 'k' and 'linear'
which state how many hash functions to use and whether to use
universal hash functions (linear=False) or general purpose hash
functions (linear=True). The hash factory can also be overridden
after instantiation by assigning the member _hash.

The EHT always starts unpruned.
The prune() method takes all arguments needed for pruning,
that is the online maxload (number of entries allowed
per online bucket), the sram word size in bits, the maximum
allowed online counter value, and the desired compression rate.
All arguments have reasonable defaults. During pruning each entry's
target online bucket is computed, and the target bucket's
load calculated. If insertion of the item would exceed
the maximum allowed load (maxload) the entry is diverted to CAM
by setting it's h value to m (out of range). Else the entry's
h value is set to the target online bucket.

This method ensures, that each entry's h value can be
used to identify whether an entry resides in CAM, a specific
online bucket, or is only offline regarding a specific bucket.

Relocation of entries during updates work equally.
"""

from math2 import *
from bloom import *
from compress import *
from xhash import *
from random import randint


__all__ = ["EfficientHashTable"]


class EfficientHashTable ():
    """Efficient Hash Table implementation

    Conceptual implementation of an Efficient Hash Table.
    The structure starts being unpruned.
    
    Members:
    _cbf            --          the counting bloom filter
    _hash           --          the hash factory
    _querycount     --          a query counter
    _putfn          --          pointer to the put method
    _getfn          --          pointer to the get method
    _popfn          --          pointer to the pop method
    """
    class Entry ():
        """Entries for efficient hash tables

        Members:
        k   --      the key
        v   --      the value
        h   --      the hash used to store the entry

        self.h is evaluated during pruning and updates
        to identify where the entry is stored in the online
        table or cam. It is updated on relocations.
        """
        def __init__ (self,k,v=None,h=None):
            """Instantiate an entry

            k   --  key
            v   --  value
            h   --  hash
            """
            self.k = k
            self.v = v
            self.h = h

        def __repr__ (self):
            return "Entry(%s,%s,%s)" %(self.k,self.v,self.h)

        def __hash__ (self):
            return hash(self.k)

        def __cmp__ (self,other):
            return cmp(hash(self),hash(other))

    def __init__ (self,n,c,linear=False,k=0):
        """Instantiate an Efficient Hash Table

        see
        "Packet Forwarding using Improved Bloom Filters", Thomas Zink, Master Thesis,
        University of Konstanz, 2009
        for a detailed description.

        Arguments:
        n           --      number of items to put into table
        c           --      number of buckets to reserve per item
        linear      --      linear growth of size m
        k           --      number of hash functions to use
        bwidth      --      number of items allowed per bucket, bucket-width
        wsize       --      the on-chip memory word size in bits
        cmax        --      maximum allowed online counter value
        """
        hfact = UniversalHashFactory
        if linear: hfact = GeneralPurposeHashFactory
        self._cbf = CountingBloomFilter(n,c,linear)
        self._hash = hfact(self._cbf._k,self._cbf._m)
        self._tbl = [[] for i in xrange(self._cbf._m)]
        self._pruned = False
        self._fp = 0
        self._querycount = 0
        self._putfn = self.__putb
        self._popfn = self.__popb
        self._getfn = self.__getb

    def __repr__ (self):
        return "EfficientHashTable(%s,%s)" % (str(self._cbf),str(self._hash))

    def put (self,key,val):
        """put(key,val) -> insert key and value and return hash,counter and position

        a wrapper for self._putfn which points to the actual put method
        """
        return self._putfn(key,val)

    def get (self,key):
        """get(key) -> returns associated value

        a wrapper for self._getfn which points to the actual get method
        """
        self._querycount += 1
        return self._getfn(key)

    def pop (self,key):
        """pop(key) -> removes key and returns associated value

        a wrapper for self._popfn which points to the actual get method
        """
        return self._popfn(key)

    def __hci (self,key):
        """__hci(key) -> h = hash(key), c = cbf[h], i = h[index(min(c))]

        calculates hashes, corresponding counters and the leftmost bucket
        with smallest counter value for key
        """
        h = self._hash(key)
        c = self._cbf.get(*h)
        i = h[c.index(min(c))]
        return h,c,i

    def __putb (self,key,val):
        """__putb(key,val) -> insert key and value and return hash,counter and position

        basic put for basic fast hash tables
        """
        hashs,counter,mlb = self.__hci(key)
        # first try if key already present and update val
        try:
            i = self._tbl[mlb].index(key)
            self._tbl[mlb][i].v = val
        # if not do insertion
        except (ValueError):
            e = self.Entry(key,val,mlb)
            for h in hashs:
                self._tbl[h].append(e)
                self._cbf.inc(h)
        return hashs,counter,mlb

    def __getb (self,key):
        """__getb(key) -> returns associated value

        basic get for basic fast hash tables
        """
        hashs,counter,mlb = self.__hci(key)
        val = None
        if min(counter) != 0:
            try:
                index = self._tbl[mlb].index(key)
                val = self._tbl[mlb][index].v
            except (ValueError):
                self._fp += 1
        return val

    def __popb (self,key):
        """__popb(key) -> removes key and returns associated value

        basic pop for basic fast hash tables
        """
        hashs,counter,mlb = self.__hci(key)
        val = None
        if min(counter) != 0:
            for h in hashs:
                try:
                    val = self._tbl[h].pop(self._tbl[h].index(key)).v
                    self._cbf.dec(h)
                except (ValueError): pass
        return val

    def prune (self,maxload = 3,wsize = 128,cmax = 5,rate = 0):
        """prune(bwidth,wsize,cmax,rate) -- prune the EHT

        Prunes the table and compresses the counting bloom filter
        using the passed arguments. The table is not really
        pruned and split into offline/online table. Instead
        online entries are identified using their h member.

        @maxload        --      maximum bucket load, number of entries per online bucket
        @cmax           --      maximum allowed online counter value
        @wsize          --      on-chip memory word size in bits
        @rate           --      target compression rate, if 0: computed
        """
        self._maxload = maxload
        self._cbf = HuffmanCountingBloomFilter (self._cbf,wsize,cmax,rate)
        # prune
        for i in xrange(self._cbf._m):
            bucket = self._tbl[i]
            self.__relocate(*bucket)
        # set function pointers
        self._putfn = self.__putp
        self._popfn = self.__popp
        self._getfn = self.__getp
        return True

    def fp (self):
        """fp() -> (float,float), returns the false positive probability and rate"""
        fpprob = self._cbf.p_fp()
        fprate = self._fp / float(self._querycount)
        return fpprob,fprate

    def distributions (self):
        """distributions() -> calculates and returns all distributions

        includes counter distribution, item distribution and cam size
        """
        cbfdist = self._cbf.distribution()
        onldist = {}
        camsize = 0
        for i in xrange(len(self._tbl)):
            bucket = self._tbl[i]
            load = 0
            cam = 0
            for entry in bucket:
                if entry.h == i: load += 1
                elif entry.h == self._cbf._m: cam += 1
            try: onldist[load] += 1
            except (KeyError): onldist[load] = 1
            camsize += cam
        return cbfdist,onldist,camsize

    def __putp (self,key,val):
        """__putp(key,val) -> insert key and value and return hash,counter and position

        pruned put for pruned efficient hash table
        """
        hashs,counter,mlb = self.__hci(key)
        # if key already present only update val
        try:
            i = self._tbl[mlb].index(key)
            self._tbl[mlb][i].v = val
        # else do insertion
        except (ValueError):
            #initialize relocation list
            e = self.Entry(key,val,mlb)
            reloc = [e]
            # insert into off
            for h in hashs:
                # get relocation entries
                for t in self._tbl[h]:
                    # online entries are inserted at front
                    if t.h == h: reloc.insert(0,t)
                    # cam entries are appended
                    elif t.h == self._cbf._m: reloc.append(t)
                self._tbl[h].append(e)
                self._cbf.inc(h)
            self.__relocate(*reloc)
        return hashs,counter,mlb

    def __getp (self,key):
        """__getp(key) -> returns associated value

        pruned get for pruned efficient hash table
        """
        hashs,counter,mlb = self.__hci(key)
        val = None
        if min(counter) != 0:
            try:
                index = self._tbl[mlb].index(key)
                e = self._tbl[mlb][index]
                # if entry is in online table or cam, return val
                if e.h == mlb or e.h == self._cbf._m: val = e.v
            except (ValueError):
                self._fp += 1
        return val

    def __popp (self,key):
        """__popb(key) -> removes key and returns associated value

        pruned pop for pruned efficient hash table
        """
        hashs,counter,mlb = self.__hci(key)
        val = None
        if min(counter) != 0:
            reloc = []
            # delete entries
            for h in hashs:
                try:
                    val = self._tbl[h].pop(self._tbl[h].index(key)).v
                    self._cbf.dec(h)
                    reloc += self._tbl[h]
                except (ValueError): pass
            self.__relocate(*reloc)
        return val

    def __relocate (self,*rlist):
        """__relocate(*rlist) -- relocate entries in rlist

        Relocations works as follows.
        All entries in rlist are hashed, counters and mostleft
        minimum bucket computed. Then conditions are checked
        to identify whether the entry is online or cam. That is
        compute online bucket load and check minimum counter
        against cmax. The entries h member is then set
        either to the most left bucket with minimum counter
        value (online entry) or to self._m (CAM entry)
        """
        for r in rlist:
            h,c,i = self.__hci(r.k)
            # compute online bucket load
            load = sum(map(lambda x: 1 if x.h == i else 0,self._tbl[i]))
            # check conditions
            if min(c) >= self._cbf._cmax or load >= self._maxload: r.h = self._cbf._m
            else: r.h = i
