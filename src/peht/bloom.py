"""Classes and functions for Bloom filters and derivates.

Implements the following classes:
Bloom                       --      an abstract class to provide a common Bloom filter interface
BloomFilter                 --      standard Bloom filter, true/false cells
CountingBloomFilter         --      counting Bloom filter, uses m counters
HuffmanCountingBloomFilter  --      a Huffman compressed counting Bloom filter


Implements an 'abstract' empty Bloom filter object
and different Bloom filter types.
"""

__all__ = ["Bloom","BloomFilter","CountingBloomFilter","HuffmanCountingBloomFilter","mbf","mfht","kbf"]

from math2 import *
from compress import *


##########################################
# functions
##########################################

def mbf (c,n):
    """mbf(c,n) -> c*n"""
    return int(c*n)

def mfht (c,n):
    """mfht(c,n) -> 2**(log(cn,2))"""
    return int(2**ceil(log(c*n,2)))

def kbf (n,m):
    """kbf(n,m) -> m/n ln 2"""
    return (m/float(n)) * log(2) 


##########################################
# 'abstract' bloom class
# parent of all bloom filter classes
##########################################

class Bloom ():
    """Empty bloom filter parent class.

    This class provides a common Interface to Bloom filters.
    It calculates all members depending on parameters and functions.
    Members:
    _n      --  number of elements
    _c      --  number of cells per element
    _m      --  number of cells, size
    _k      --  number of hash functions
    _linear --  the function used to calculate _m
    """
    def __init__ (self,n,c,linear = True,k = 0):
        """Instatiate a bloom object.

        @n      --  number of elements to store
        @c      --  number of reserved cells per element
        @linear --  True if size is computed linear, False if to power 2
        @k      --  Number of hash functions, if 0, optimum is computed
        """
        self._n = n
        self._c = c
        self._linear = linear
        self._m = mbf(c,n) if linear else mfht(c,n)
        k = kbf(self._n, self._m) if k <= 0 else k
        self._k = int(round(k)) if k >= 2 else 2

    def __repr__ (self):
        """__repr__() -> string"""
        return "Bloom(%s,%s,%s,%s)" % (self._n,self._c,self._linear,self._k)

    def __len__ (self):
        """__len__() -> self._m"""
        return self._m
    
    def p_0 (self):
        """p_0() -> float

        probability that any cell has 0 insertions
        """
        return (1 - 1/float(self._m))**(self._k*self._n)

    def p_i (self,i):
        """p_i(i) -> float

        probability that any cell has i insertions
        """
        return comb(int(mul(self._n,self._k)),i) * pow(1/float(self._m),i) * pow(1-1/float(self._m),mul(self._n,self._k)-i)

    def p_fp (self):
        """p_fp() -> float

        false positive probability
        """
        return pow(1-pow(1 - 1/float(self._m), self._n*self._k),self._k)

    """
        probability that the first r hashes produce j distinct values
        d(j,r) in fht paper
    """
    def p_distinct (self,j,r):
        """p_distinct -> float

        probability that r hashes produce j distinct values
        """
        if j>r: return 0
        if j==0 and r==0: return 1
        if j==0 and r>0: return 0
        return j/float(self._m) * self.p_distinct(j,r-1) + ((self._m - j + 1) / float(self._m)) * self.p_distinct(j-1,r-1)

    def p_countervalue(self,i,j):
        """p_countervalue(i,j) -> float
        
        probability that a counter in a set of j non-empty buckets is j
        p(i,j) in fht paper
        """
        return comb((self._n * self._k -j),(i-1)) * (1/float(self._m))**(i-1) * (1-1/float(self._m))**((self._n*self._k-j)-(i-1))

    def p_smallest (self,r,s,j):
        """p_smallest(r,s,j) -> float

        probability that smallest counter in any r of j buckets is s
        q(r,s,j) in fht paper
        """
        result = 0
        for i in xrange(1,r+1):
            x = comb(r,i) * self.p_countervalue(s,j)**i
            y = 0
            for h in xrange(1,s+1):
                y += self.p_countervalue(h,j)
            result += x * (1-y)**(r-i)
        return result

    def p_linkedlist (self,s):
        """p_linkedlist(s) -> float

        probability that the smallest value for any k'<= k chosen
        cells has value s
        """
        x = 0
        for j in xrange(1,self._k+1):
            x += self.p_distinct(j,self._k) * self.p_smallest(j,s,j)
        return x

    def e_maxload (self):
        """e_maxload() -> the expected maximum load"""
        return log(log(self._m)) / log(self._k) if self._k > 1 else log(self._n)/log(log(self._n))

    def e_dist (self,upper):
        """expecteddist(upper) -> [int]

        the expected distribution of counter values or insertions
        """
        expc = {}
        for i in xrange(upper):
            expc[i] = round(self.p_i(i) * self._m)
        return expc

    def size (self):
        """size() -> self._m -- same as len(bloom)"""        
        return self._m

    def get (self,*indexes):
        """Wrapper for get bits"""
        raise NotImplementedError("Function not implemented")

    def set (self,*indexes):
        """Wrapper for set bits"""
        raise NotImplementedError("Function not implemented")

    def clear (self,*indexes):
        """Wrapper for clear bits"""
        raise NotImplementedError("Function not implemented")

    def inc (self,*indexes):
        """Wrapper for incrementing counters"""
        raise NotImplementedError("Function not implemented")

    def dec (self,*indexes):
        """Wrapper for decrementing counters"""
        raise NotImplementedError("Function not implemented")
    
##########################################
# bloomfilter class
# Implements a standard Bloom filter
##########################################

class BloomFilter (Bloom):
    """Standard Bloom filter class

    Implements a standard Bloom filter.
    m cells with True / False.
    Child of bloom().
    Members:
    Bloom.*     --  inherited from Bloom class
    _bf         --  the actual bloom filter, implemented as [ bool ]
    """
    def __init__ (self,n,c,linear = True,k = 0):
        """Instatiate a Bloom filter object.

        @n      --  number of elements to store
        @c      --  number of reserved cells per element
        @linear --  True if size is computed linear, False if to power 2
        @k      --  Number of hash functions, if 0, optimum is computed
        """
        Bloom.__init__(self,n,c,linear,k)
        self._bf = [False] * self._m

    def __repr__ (self):
        """__repr__() -> string"""
        return "BloomFilter(%s,%s,%s,%s)" % (self._n,self._c,self._linear,self._k)
    
    def set (self,*indexes):
        """set(*indexes) -- set bits at positions in [indexes]"""
        for i in indexes: self._bf[i] = True
    
    def clear (self,*indexes):
        """clear(*indexes) -- clear bits at positions in [indexes]"""
        for i in ind: self._bf[i] = False
    
    def get (self,*indexes):
        """get(*indexes) -> [bool for i in indexes]"""
        return [self._bf[i] for i in indexes]

    def inc (self,*indexes):
        """inc(*indexes) -- same as set([indexes])"""
        self.set(*indexes)

    def dec (self,*indexes):
        """dec(*indexes) -- same as clear([indexes])"""
        self.clear(*indexes)

##########################################
# Counting Bloom Filter class
# Implements a counting Bloom filter
##########################################

class CountingBloomFilter (Bloom):
    """Implements a counting Bloom filter

    Uses a sequence of ints as counters instead of bools.
    No bit tricks to actually use a certain amount of bits.
    Memory management left to python engine.
    Members:
    Bloom.*         --      inherited from Bloom class
    _bf             --      the counter sequence, implemented as [ int ]
    """
    def __init__ (self,n,c,linear = True,k = 0):
        Bloom.__init__(self,n,c,linear,k)
        self._bf = [0] * self._m

    def __repr__ (self):
        """__repr__() -> string"""
        return "CountingBloomFilter(%s,%s,%s,%s)" % (self._n,self._c,self._linear,self._k)

    def inc (self,*indexes):
        """inc(*indexes) -- increment counters at positions in [indexes]"""
        for i in indexes: self._bf[i] += 1

    def dec (self,*indexes):
        """dec(*indexes) -- decrement counters at positions in [indexes]"""
        for i in indexes: self._bf[i] -= 1

    def get (self,*indexes):
        """get(*indexes) -> [int for i in indexes]"""
        return [self._bf[i] for i in indexes]

    def set (self,*indexes):
        """set(*indexes) -- same as inc([indexes])"""
        self.inc(*indexes)

    def clear (self,*indexes):
        """clear(*indexes) -- same as dec([indexes])"""
        self.dec(*indexes)

    def size (self, cwidth = 0):
        """size(cwidth) -> m * cwidth if cwidth > 0 else m * log(max(bf,2))

        Calculates the number of bits needed for the counting Bloom filter.
        Uses cwidth as counter width, ie, number of bits per counter.
        If cwidth == 0, the counter width is calculated by evaluating
        the maximum counter value in the filter.
        """
        ret = self._m
        if cwidth == 0:
            if max(self._bf) > 1: ret = mul(self._m, add(floor(log(max(self._bf),2)),1)) 
        else:
            ret = self._m * cwidth
        return ret

    def distribution (self):
        """distribution() -> { value : frequency }

        Calculate a histogram of all counter values.
        Basically a wrapper for compressed cbfs, where
        distribution must be calculated individually.
        """
        return hist(self._bf)

    def maxcounter (self):
        """maxcounter() -> max([bf])

        Calculate the maximum counter value.
        Basically a wrapper function for compressed cbfs
        """
        return max(self._bf)

##########################################
# Huffman Compressed Counting Bloom Filter class
# Implements a huffman compressed counting Bloom filter
##########################################

class HuffmanCountingBloomFilter (Bloom):
    """Implements a Huffman compressed counting Bloom filter

    The Huffman compressed counting Bloom filter uses Huffman
    compression to encode the counter values. For easy indexing
    a fixed number of counters is compressed into one word
    of fixed size.
    Members:
    Bloom.*             --          inherited from Bloom
    _huff               --          Huffman compressor
    _wsize              --          word size
    _cmax               --          maximum counter value
    _rate               --          compression rate
    _overmax            --          counter values that exceed _cmax { index : value }
    _bf                 --          the compressed counters
    """
    def __init__ (self,cbf,wsize,cmax,rate = 0):
        """Instantiate a Huffman compressed counting Bloom filter

        cbf     --      an uncompressed counting Bloom filter
        wsize   --      word size in bits
        cmax    --      maximum allowed counter value
        rate    --      compression rate, if 0, it is calculated using soft compression
        """
        if not isinstance(cbf,Bloom):
            raise TypeError, "Argument cbf must be of type Bloom"
        Bloom.__init__(self,cbf._n,cbf._c,cbf._linear,cbf._k)
        self._wsize = wsize
        self._cmax = cmax
        self._huff = Huffman(cbf.e_dist(self._cmax+1))
        self._overmax = {}
        if rate > 0: self._compress_hard(cbf._bf)
        else : self._compress(cbf._bf)

    def __repr__ (self):
        """__repr__() -> string"""
        return "HuffmanCountingBloomFilter(%s,%s,%s,%s,%s,%s,%s)" % (self._n,self._c,self._linear,self._k,self._wsize,self._cmax,self._rate)

    def _compress (self,bf):
        """_compress(bf) -- calculates rate and compresses counters

        Compresses a bloom filter using soft compression.
        Iteratively tries to fit as many counters into a word as possible.
        If on subsequent words, less counters can be put into
        a word, the algorithm starts new with smaller rate.
        """
        compressed = []
        rate = self._wsize
        word = []
        i = 0
        # iteratively find the compression rate
        while i < len(bf):
            plain = bf[i]
            # check for counter overflows resp. cmax
            if plain > self._cmax:
                self._overmax[i] = plain
                plain = self._cmax
            # do encoding
            code = self._huff.encode(plain) [0]   
            i += 1
            entries = len(word)
            if entries < rate:
                word.append(code)
                wsize = sum(map(len,word))
                if wsize > self._wsize:
                    compressed = []
                    word = []
                    rate = entries
                    i = 0
            else:
                compressed.append(word)
                word = [code]
        # finalize and set members
        if word != []: compressed.append(word)
        self._rate = rate
        self._bf = compressed

    def _compress_hard (self,bf):
        """_compress_hard(bf) -- uses predefined rate to compresses counters

        Compresses a bloom filter using hard compression.
        Compression rate is predefined. _wsize is ignored.
        It is cheaper, to calculate word overflows on demand,
        rather than keep updating on ever word access.
        """
        compressed = []
        word = []
        i = 0
        # compress self._rate codes and put overflown to overflow list
        while i < len(bf):
            plain = bf[i]
            if plain > self._cmax:
                self._overmax[i] = plain
                plain = self._cmax
            code = self._huff.encode([plain]) [0]
            if len(word)+1 >= self._rate:
                compressed.append(word)
                word = [code]
            else: word.append(code)
        compressed.append(word)
        self._bf = compressed

    def inc (self,*indexes):
        """inc(*indexes) -- increment counters at positions in [indexes]"""
        for i in indexes:
            # find the index to compressed counter
            a = i / self._rate
            b = i - a * self._rate
            # decode
            de = self._huff.decode(self._bf[a][b])[0] + 1
            # check for overflow
            if de <= self._cmax: 
                self._bf[a][b] = self._huff.encode(de)[0]
            else:
                try: self._overmax[i] = self._overmax[i] + 1
                except (KeyError): self._overmax[i] = de

    def dec (self,*indexes):
        """dec(*indexes) -- decrement counters at positions in [indexes]"""
        for i in indexes:
            # find the index to compressed counter
            a = i / self._rate
            b = i - a * self._rate
            # decode
            de = self._huff.decode(self._bf[a][b]) [0]
            # check for overflow
            if de >= self._cmax:
                try: self._overmax[i] = self._overmax[i] - 1
                except (KeyError): pass
            else:
                self._bf[a][b] = self._huff.encode(de-1) [0]

    def get (self,*indexes):
        """get(*indexes) -> [int for i in indexes]"""
        return self._huff.decode(*[self._bf[i/self._rate][i-(i/self._rate)*self._rate] for i in indexes])

    def set (self,*indexes):
        """set(*indexes) -- same as inc([indexes])"""
        self.inc(*indexes)

    def clear (self,*indexes):
        """clear(*indexes) -- same as dec([indexes])"""
        self.dec(*indexes)

    def size (self):
        """size() -> compressed filter size, number of words * word size"""
        return len(self._bf) * self._wsize

    def usize (self,cwidth=0):
        """size() -> uncompressed filter size, by word"""
        usize = 0
        if cwidth == 0:
            maxc = max(concat(self._cmax,self._overmax.values()))
            if maxc <= 1: cwidth = 2
            else: cwidth = add(floor(log(maxc,2)),1)
        numuwords = ceil((self._m * cwidth) / float(self._wsize))
        usize = numuwords * self._wsize

    def psize (self):
        """psize() -> filter size if compressed using word packing"""
        p = Packer(self._wsize,range(0,self._cmax+1))
        return p.size(self._m)

    def distribution (self):
        """distribution() -> { value : frequency }"""
        dist = {}
        index = 0
        for i in reduce(add,self._bf):
            c = self._huff.decode(i)[0]
            if c == self._cmax:
                try: c = self._overmax[index]
                except (KeyError): pass
            try: dist[c] += 1
            except (KeyError): dist[c] = 1
            index += 1
        return dist

    def maxcounter (self):
        """maxcounter() -> max([bf])

        Calculate the maximum counter value.
        Basically a wrapper function for compressed cbfs
        """
        bf = self._huff.decode(*(reduce(add,self._bf)))
        maxc = self._overmax.values()
        maxc.append(max(bf))
        return max(maxc)

    def overflows (self):
        """overflows() -> int, return number of counter overflows"""
        return len(self._overmax)
