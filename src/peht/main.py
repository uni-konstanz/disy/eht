"""Efficient Hash Table Simulation

Main file of the EHT simulation framework.
Unifies all classes and provides argument checking.

The simulator framework consists of the following modules:

abstract            --      implements abstract bases classes and methods
math2               --      additional math functions
bloom               --      Bloom filters and derivates
compress            --      compression interface and classes
hash                --      hash functions and factories
eht                 --      efficient hash table implementation
simulator           --      configurator and simulator classes for EHT simulation

See individual documentations of the modules for more information.

Output is generated both on stdout and stderr.
stdout yields the results which can directly be written to a csv file.
stderr shows runtime and error information which can be diverted to /dev/null


Requirements
------------
The framework is built on python 2.5 / 2.6. It does NOT run
on python 3.0 or higher due to substantial changes in some function
definitions.

Usage
-----
python main.py [options]

Command line Options
--------------------
-n
    Sets the number of items to insert into the EHT.
    The EHT is built accordingly to n.

-c
    The table size constant. The size of an FHT and EHT
    is calculated using the equation:
    m = 2 ** (log(c*n,2))
    c thus gives the number of counters/buckets per item.
    Default: 1.6

-k
    The number of hash functions to use. Defaults to 0
    in which case the optimal number is calculated by
    k = m/n * log(2)

-w
    Specify the maximum bucket load / width for online
    table buckets. Default: 4

-s
    Sram word size in bits. Default: 128
    Primarily influences the compression quality

-l
    Use linear table growth instead of multiples of two.
    That is calculate size m using equation:
    m = c * n
    If -l is set, universal hash functions can't be
    used, since they always produce multiples of two.
    Instead k general purpose hash functions are
    randomly selected and a HashFactory built.

-r
    Set the desired compression rate, number of counters
    compressed in one sram memory word. Defaults to 0
    in which case the optimum is computed during pruning.

-f
    Set a replacement factor 0 < f <= 1 which is the number
    of items in the table which get replaced after pruning.
    This is to test update performance.

-t
    set the number of tries, simulation runs. Default: 10

Examples
--------
> python main.py -n 10000

    build an EHT for 10.000 items with default other parameters

> python main.py -n 100000 -c 3.2 -w 2 -s 64

    build an EHT for 100.000 items, 3.2 counters per item a maxload of 2
    entries per online bucket and an sram word size of 64 bits.

> python main.py -n 1000000 -c 1.6 -w 3 -s 64 -l >> result.csv 2> /dev/null

    build an EHT for 1 million items with 1.6 buckets per item and linear function
    for m. Sram word size is 64 bits. Print results to the file result.csv and
    divert error output to /dev/null.
"""

import abstract as abc
import simulator as simul
import eht
import xhash as hsh
import bloom
import compress as cmpr
import math2 as m2
import time, sys, getopt


__all__ = ["checkargv","usage"]


def checkargv (argv):
    """checkargv() -> Configurator,int,float

    Checks all command line arguments, builds and
    returns a Configurator for the Simulator and the number
    of tries and a replacement factor"""
    # print usage
    if argv == []: usage()
    # initialize defaults
    n = 1000
    c = 1.6
    linear = False
    hfact = None
    maxload = 4
    wsize = 128
    cmax = 5
    rate = 0
    k = 0
    trials = 10
    f = 0.5
    # get options
    try: opts,args = getopt.getopt(argv,"n:m:c:k:w:s:r:t:f:hl", ["help"])
    except (getopt.GetoptError): usage()
    for opt, arg in opts:
        if opt == "-n": n = int(arg)
        elif opt == "-m": cmax = int(arg)
        elif opt == "-c": c = float(arg)
        elif opt == "-k": k = int(arg)
        elif opt == "-w": maxload = int(arg)
        elif opt == "-s": wsize = int(arg)
        elif opt == "-l": linear = True
        elif opt == "-r": rate = int(arg)
        elif opt == "-f": f = float(arg)
        elif opt == "-t": trials = int(arg)
        elif opt in ("-h","--help"): usage()
    # instantiate and return
    cfg = simul.Configurator(n,c,linear,k,maxload,wsize,cmax,rate)
    return cfg,trials,f

def usage ():
    """usage() -- print help screen"""
    print "usage: python simulator.py [options]"
    print
    print "\t-n: number of items"
    print "\t-c: multiplier constant, number of counters/buckets per item"
    print "\t-k: set the number of universal hash functions to use"
    print "\t-m: maximum online counter value allowed"
    print "\t-w: maximum allowed bucket load / width of online table"
    print "\t-s: sram wordsize in bits"
    print "\t-l: use linear table growth and general purpose hash functions"
    print "\t-f: replacement factor, how many of n items will be replaced, 0 < f < 1, default 0.5"
    print "\t-t: number of trials / simulation runs"
    sys.exit(0)

def main (argv):
    """main(argv) -> Simulator

    Check command line arguments. Instantiate a new Simulator
    and run all simulations.
    """
    cfg,trials,f = checkargv(argv)
    sim = simul.Simulator(cfg,trials,f)
    sim.run()
    return sim

if __name__=="__main__":
    sim = main(sys.argv[1:])
