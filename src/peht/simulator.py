"""EHT simulator framework

This simulator implements command line arguments
and configurations for EHT simulations.
The configurator takes all arguments needed
to instantiate and prune an EHT. The simulator
takes a configurator instance, a number of
simulations 'tries' and a replacement factor f as arguments.
For each 'try', a new EHT is instantiated and simulations
are performed as follows.

Simulations:
for all s in xrange(tries):
    Create new EHT
    fill with n items.
    pruned
    n*f items are removed
    n*(1-f) items are inserted
    query for all n plus additional random n items
    All statistical information is calculated and printed on stdout

Classes:
Configurator        --          configurator class for the simulator
Simulator           --          EHT simulator class
"""

__all__ = ["Configurator","Simulator"]

from eht import *
from compress import *
from random import randint
import time, sys, getopt

class Configurator ():
    """Configurator implementation

    simply sets all arguments needed to instantiate
    and prune an EHT.
    """
    def __init__ (self,n,c,linear=False,k=0,maxload=4,wsize=128,cmax=5,rate=0):
        """Instantiate a Configurator

        @n          --          number of items
        @c          --          number of buckets/counters per item
        @linear     --          linear growth true or false
        @hfact      --          a hash factory used for hashing
        @maxload    --          maximum allowed online bucket load
        @wsize      --          sram word size in bits
        @cmax       --          maximum allowed online counter values
        @rate       --          desired compression rate, if 0 automatically computed
        """
        self._n = n
        self._c = c
        self._linear = linear
        self._k = k
        self._maxload = maxload
        self._wsize = wsize
        self._cmax = cmax
        self._rate = 0

    def __repr__ (self):
        return "Configurator(%s,%s,%s,%s,%s,%s,%s,%s)" % (self._n,self._c,self._linear,self._hfact,self._maxload,self._wsize,self._cmax,self._rate)

class Simulator ():
    """EHT Simulator implementation

    Takes a Configurator and a number of simulations as arguments.
    Members:
    _cfg                --          the Configurator instance
    _tries              --          number of tries / simulations
    _f                  --          replacement factor, how many items of n get replaced
    _eht                --          the current (and final) eht instance
    _kv                 --          key / value dictionart of all entries in _eht
    _crng               --          the range of counters to include in results
    """
    def __init__ (self,cfg,tries,f=0.5,crng=10):
        """Instantiate a Simulator

        @cfg        --          a configurator instance
        @tries      --          the number of simulation runs
        @f          --          replacement factor, what fraction of n gets replaced
        """
        if not isinstance(cfg,Configurator):
            raise TypeError("Argument cfg must be a Configurator instance")
        if type(tries) != type(0):
            raise TypeError("Argument tries must be an integer")
        self._cfg = cfg
        self._tries = tries
        self._f = f
        self._crng = crng

    def __repr__ (self):
        return "Simulator(%s,%s,%s)" % (self._cfg,self._tries,self._f)

    def run (self):
        """run() -- run simulations and print output"""
        total = time.time()
        self.__printheader()
        for s in xrange(self._tries):
            print >> sys.stderr, "Try %s of %s" % (s+1,self._tries)
            self._eht = EfficientHashTable(self._cfg._n,self._cfg._c,self._cfg._linear,self._cfg._k)
            self._kv = {}
            start = time.time()
            self.__insert(self._cfg._n)
            self.__prune()
            self.__remove(self._cfg._n*self._f)
            self.__insert(self._cfg._n*(1.0-self._f))
            self.__query(True)
            stop = time.time()
            print >> sys.stderr,"Runtime: %s sec" % (stop - start)
            self.__result()
        total = time.time() - total
        print >> sys.stderr,"Total runtime: %s sec" % (total)

    def __printheader (self):
        """printheader() -- prints the header for simulations on stdout

        The header includes the following information:
        n,c,m,k,cmax,maxload,wsize          --          EHT configuration
        urate,prate,crate                   --          uncompressed, packet, huffman compressed rate (counters per word)
        usize,psize,csize                   --          uncompressed, packet, huffman compressed summary size in bits
        minfill,maxfill,avgfill             --          how many bits per word are used for encoding counters, min, max, average
        maxcounter,overflow                 --          maximum counter value and number of overflown counters in offline summary
        fpprob,fprate                       --          the false positive probability and rate (actual ratio of fps to queries)
        bXX                                 --          distribution of number of bits used for codes
        c0-c9                               --          counter distribution between value 0 and 9
        t0-tX                               --          online entry distribution
        camsize                             --          number of entries in CAM
        """
        header = "n,c,m,k,cmax,maxload,wsize,urate,prate,crate,usize,psize,csize,minfill,maxfill,avgfill,maxcounter,overflows,fpprob,fprate"
        lowestpossiblefill = self._cfg._wsize / self._cfg._cmax
        for i in xrange(lowestpossiblefill,self._cfg._wsize+1): header += ",b%s" % i
        for i in xrange(self._crng): header += ",c%s" % i
        for i in xrange(self._cfg._maxload+1): header += ",t%s" % i
        header += ",camsize"
        print header
        return header

    def __prune (self):
        """prune() -- a wrapper for eht.prune() to print runtime info"""
        start = time.time()
        self._eht.prune(self._cfg._maxload,self._cfg._wsize,self._cfg._cmax,self._cfg._rate)
        stop = time.time()
        print >> sys.stderr,"pruned in %s sec" % (stop - start)
        
    def __insert (self,n):
        """insert(n) -- inserts n items into eht and updates the kv dictionary"""
        start = time.time()
        for i in xrange(0,int(n)):
            key = randint(2**16,2**48-1)
            val = randint(0,2**16-1)
            self._eht.put(key,val)
            self._kv[key] = val
        stop = time.time()
        print >> sys.stderr,"inserted %s items in %s sec" % (n,(stop - start))

    def __remove (self,n):
        """remove(n) -- removes n items into eht and updates the kv dictionary"""
        start = time.time()
        errors = 0
        for i in xrange(0,int(n)):
            key = self._kv.keys()[randint(0,len(self._kv)-1)]
            val = self._eht.pop(key)
            if val != self._kv[key]:
                errors += 1
                print >> sys.stderr,"Error: %s -> %s != %s" % (key,val,self._kv[key])
                h,c,i = self._eht._EfficientHashTable__hci(key)
                print >> sys.stderr, "%s,%s,%s" % (h,c,i)
                tbl = [self._eht._tbl[i] for i in h]
                for i in tbl:
                    print >> sys.stderr, i
            self._kv.pop(key)
        stop = time.time()
        print >> sys.stderr,"removed %s items in %s sec with %s errors" % (n,(stop - start),errors)

    def __query (self,rand=False):
        """query(rands) -- query eht for all keys in _kv, if rands=True also query for n randoms"""
        n=self._cfg._n
        start = time.time()
        errors = 0
        queried = 0
        for key in self._kv.keys():
            val = self._eht.get(key)
            if val != self._kv[key]:
                errors += 1
                print >> sys.stderr,"Error: %s -> %s != %s" % (key,val,self._kv[key])
                h,c,i = self._eht._EfficientHashTable__hci(key)
                print >> sys.stderr, "%s,%s,%s" % (h,c,i)
                tbl = [self._eht._tbl[i] for i in h]
                for i in tbl:
                    print >> sys.stderr, i
            queried += 1
        if rand:
            for i in xrange(0,2*n):
                key = randint(2**16,2**48-1)
                self._eht.get(key)
                queried += 1
        stop = time.time()
        print >> sys.stderr,"queried %s items in %s sec with %s errors" % (queried,(stop - start),errors)
        
    def __result(self):
        """result() -- calculate and print results"""
        csize = 3.0
        p = Packer(self._cfg._wsize,xrange(0,self._cfg._cmax+1))
        # n,c,m,k,cmax,maxload,wsize
        result = "%s,%s,%s,%s,%s,%s,%s" % (self._cfg._n,self._cfg._c,self._eht._cbf._m,len(self._eht._hash),self._cfg._cmax,self._cfg._maxload,self._cfg._wsize)
        # urate,prate,crate
        urate = self._cfg._wsize / csize
        prate = p._rate
        crate = self._eht._cbf._rate
        result += ",%s,%s,%s" % (urate,prate,crate)
        # usize,psize,csize
        usize = self._eht._cbf._m * csize
        psize = p.size(self._eht._cbf._m)
        csize = self._eht._cbf.size()
        result += ",%s,%s,%s" % (usize,psize,csize)
        # minfill,maxfill,avgfill
        wordfills = [sum(map(len,self._eht._cbf._bf[i])) for i in xrange(len(self._eht._cbf._bf))]
        numwords = len(self._eht._cbf._bf)
        avgfill = sum(wordfills) / float(numwords)
        minfill = min(wordfills)
        maxfill = max(wordfills)
        result += ",%s,%s,%s" % (minfill,maxfill,avgfill)
        # maxcounter, fp
        maxcounter = self._eht._cbf.maxcounter()
        overflows = self._eht._cbf.overflows()
        fpprob,fprate = self._eht.fp()
        result += ",%s,%s,%s,%s" % (maxcounter,overflows,fpprob,fprate)
        # codedistribution
        codedist = {}
        for i in wordfills:
            try: codedist[i] += 1
            except (KeyError): codedist[i] = 1
        lowestpossiblefill = self._cfg._wsize / self._cfg._cmax
        for i in xrange(lowestpossiblefill,self._cfg._wsize +1):
            try: result += ",%s" % (codedist[i])
            except (KeyError): result += ",0"
        # distributions and camsize
        cbfdist, onldist, camsize = self._eht.distributions()
        for i in xrange(self._crng):
            try: result += ",%s" % cbfdist[i]
            except (KeyError): result += ",0"
        for i in xrange(self._cfg._maxload+1):
            try: result += ",%s" % (onldist[i])
            except (KeyError): result += ",0"
        result += ",%s" % camsize
        print result
        return result
