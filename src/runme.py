import os,sys
from subprocess import Popen

def main (argv):
    #N = [10000,100000,1000000]
    N = [100000]
    #C = [12.8,6.4,3.2,1.6,0.8]
    C = [6.4,3.2,1.6,0.8]
    K = [0]
    M = [4,5]
    W = [4]
    R = [64,128]
    s = 20
    p = []

    cwd = os.getcwd()
    tmp = cwd.split('/')
    tmp[len(tmp)-1] = "results"
    tmp.append('')
    results = '/'.join(tmp)

    i = 0
    for n in N:
        for c in C:
            for k in K:
                for m in M:
                    for w in W:
                        for r in R:
			    i += 1
                            outfile = "%sn%sc%sk%sm%sw%sr%s.csv" % (results,n,c,k,m,w,r)
                            outfiled = file(outfile,'w')
                            logfile = "%sn%sc%sk%sm%sw%sr%s.log" % (results,n,c,k,m,w,r)
                            logfiled = file(logfile,'w')
                            ps = ["python", cwd + "/iplookup.py", "-n %s" % n, "-c %s" % c, "-k %s" % k, "-m %s" % m, "-w %s" % w, "-r %s" % r, "-s %s" % s]
                            print "create process num %s : %s" % (i,ps)
                            #p.append(Popen(args=ps,stdout=outfiled,stderr=logfiled))
    return True

if __name__ == "__main__":
    result = main(sys.argv[1:])
