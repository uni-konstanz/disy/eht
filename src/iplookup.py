##############################################################
# imports
##############################################################
from math import *                  # math functions
from operator import *              # c wrapper operators
import sys                          # system arguments etc
import getopt                       # get command line options
import random                       # randomizer
import time                         # time functions, prime for profiling and performance 
#from threading import Thread       # high level threading
#import thread                      # low level threads
#import pylab as pyl                 # scientific functions
#import numpy as np                  # complex numbers etc
#import matplotlib.pyplot as plt     # 2d plot library, show
#import pyx                          # 2/3d plot library, pdf files

###
# Math related functions and classes
###
"""
    faculty function: compute faculty of x
"""
def fact (x): return 1 if x <= 1 else reduce(mul, xrange(2,x+1))

"""
    compute permutations of drawing r from set n
"""
def perm (n,r):
    if r<=1:
        if r == 1: return n
        if r == 0: return 1
        if r <= 0: return 0
    return reduce(mul, xrange(n-r+1,n+1))

"""
    compute combinations of drawing r from set n
"""
def comb (n,r): return div(perm(n,r),fact(r))

##############################################################
# Bloom Filters
##############################################################
def mbf (n,c): return mul(c,n)

def mfht (n,c): return int(pow(2,ceil(log(c*n,2))))

def kbf (n,m): return int(ceil(mul(div(m,float(n)),log(2))))

def p_0 (n,m,k): return (1 - 1/float(m))**(k*n)

def p_i (n,m,k,i):
    return comb(int(mul(n,k)),i) * pow(1/float(m),i) * pow(1-1/float(m),mul(n,k)-i)

def p_all(nseq,cseq,mfn,k,kfn,maxv):
    probs = []
    expect = []  
    for n in nseq:
        for c in cseq:
            b = bloom(n,c,mfn,k,kfn)
            ps = [n,b._m] + b.p_all(maxv)
            probs.append(ps)
            expect.append([n,b._m] + [i*n for i in ps[2:]])
    for x in probs:
        print str(x).strip("[]").replace(' ','')
    for x in expect:
        print str(x).strip("[]").replace(' ','')
    return probs,expect

    """        
    probs = []
    expect = []  
    for n in seq:
        m = mfn(n,c)
        k = kbf(n,m)
        probs.append([p_i(n,m,k,i) for i in xrange(maxv)])
        expect.append([i*n for i in probs[-1]])
    return probs,expect
    """

"""
    pack related functions
"""
class package:
    def __init__ (self,wsize,maxc):
        self._wsize = wsize
        self._maxc = maxc
        self._range = self._maxc + 1
        self._rate = self.packrate()
    
    def packrate (self):
        return int(floor(log(2**self._wsize,2)/log(self._range,2)))

    def unpack (self,word):
        x = []
        for i in xrange(self._rate):
            #x.insert(0,word % self._range)
            x.append(word % self._range)
            word /= self._range
        return x

    def pack (self,counters):
        #power = self._rate -1
        power = 0
        word = 0
        for i in counters:
            word += int(i * self._range**power)
            #power -= 1
            power += 1
        return word

    def packbook (self):
        x = []
        for i in xrange(self._range**self._rate):
            x.append([i,self.unpack(i),self.pack(self.unpack(i))])
        return x

    def packlist (self,l):
        numc = 0
        curr = []
        words = []
        for i in xrange(len(l)):
            curr.append(l[i])
            numc += 1
            if numc >= self._rate or i == len(l)-1:
                words.append(self.pack(curr))
                curr = []
                numc = 0
        return words
        
    def size (self,numc):
        return (numc/float(self._rate)) * self._wsize

class bloom ():
    def __init__ (self,n,c,mfn,k,kfn):
        self._n = n
        self._c = c
        self._m = mfn(n, c)
        self._k = kfn(n, self._m) if k <= 0 else k
        self._mfn = mfn
        self._kfn = kfn

    def __repr__ (self):
        return "<type 'bloom'>"

    def p_0 (self):
        return p_0(self._n,self._m,self._k)

    def p_i (self,i):
        return p_i(self._n,self._m,self._k,i)

    def p_all (self,maxv):
        return [p_i(self._n,self._m,self._k,i) for i in xrange(maxv)]

    def p_fp (self):
        return pow(1-pow(1 - 1/float(self._m), self._n*self._k),self._k)

    """
        probability that the first r hashes produce j distinct values
        d(j,r) in fht paper
    """
    def p_distinct (self,j,r):
        if j>r: return 0
        if j==0 and r==0: return 1
        if j==0 and r>0: return 0
        return j/float(self._m) * self.p_distinct(j,r-1) + ((self._m - j + 1) / float(self._m)) * self.p_distinct(j-1,r-1)

    """
        probability that a counter in a set of j non-empty buckets is j
        p(i,j) in fht paper
    """
    def p_countervalue(self,i,j):
        return comb((self._n * self._k -j),(i-1)) * (1/float(self._m))**(i-1) * (1-1/float(self._m))**((self._n*self._k-j)-(i-1))

    """
        probability that smallest counter in any r of j buckets is s
        q(r,s,j) in fht paper
    """
    def p_smallest (self,r,s,j):
        result = 0
        for i in xrange(1,r+1):
            x = comb(r,i) * self.p_countervalue(s,j)**i
            y = 0
            for h in xrange(1,s+1):
                y += self.p_countervalue(h,j)
            result += x * (1-y)**(r-i)
        return result

    """
        probability that any bucket has value s
    """
    def p_linkedlist (self,s):
        x = 0
        for j in xrange(1,self._k+1):
            x += self.p_distinct(j,self._k) * self.p_smallest(j,s,j)
        return x

    def expecteddist (self,upper):
        expc = {}
        for i in xrange(upper):
            expc[i] = round(self.p_i(i) * self._m)
        return expc

    def size (self):
        return self._m
        
class bloomfilter (bloom):
    def __init__ (self,n,c,mfn,k,kfn):
        bloom.__init__(self,n,c,mfn,k,kfn)
        self._bf = [False] * self._m
        
    def set (self,ind):
        for i in ind: self._bf[i] = True
    
    def clear (self,ind):
        for i in ind: self._bf[i] = False
    
    def get (self,ind):
        return [self._bf[i] for i in ind]
    
class countingbloomfilter (bloom):
    def __init__ (self,n,c,mfn,k,kfn):
        bloom.__init__(self,n,c,mfn,k,kfn)
        self._bf = [0] * self._m

    def inc (self,ind):
        for i in ind: self._bf[i] += 1

    def dec (self,ind):
        for i in ind: self._bf[i] -= 1

    def get (self,ind):
        return [self._bf[i] for i in ind]

    def size (self):
        maxc = max(self._bf)
        if maxc <= 1: return self._m
        return mul(self._m, add(floor(log(max(self._bf),2)),1))

    def distribution (self):
        dist = {}
        for i in self._bf:
            try: dist[i] += 1
            except (KeyError): dist[i] = 1
        return dist

    def maxcounter (self):
        return max(self._bf)

class huffmancbf (bloom):
    def __init__ (self,cbf,word,climit):
        bloom.__init__(self,cbf._n,cbf._c,cbf._mfn,cbf._k,cbf._kfn)
        self._word = word
        self._climit = climit
        self._huff = huffman(cbf.expecteddist(self._climit+1))
        self._over = {}
        self._pack = floor(log(2**self._word,2)/log(self._climit+1,2))
        self._bf = self._compress(cbf._bf)

    def inc (self,ind):
        for i in ind:
            a = i / self._crate
            b = i - a * self._crate
            de = self._huff.decode([self._bf[a][b]])[0] + 1
            if de <= self._climit: 
                self._bf[a][b] = self._huff.encode([de])[0]
            else:
                try: self._over[i] = self._over[i] + 1
                except (KeyError): self._over[i] = de

    def dec (self,ind):
        for i in ind: 
            a = i / self._crate
            b = i - a * self._crate
            de = self._huff.decode([self._bf[a][b]])[0] #- 1
            if de == self._climit:
                try: self._over[i] = self._over[i] - 1
                except (KeyError):
                    self._bf[a][b] = self._huff.encode([de-1]) [0]

    def get (self,ind):
        return self._huff.decode([self._bf[i/self._crate][i-(i/self._crate)*self._crate] for i in ind])

    def _compress (self,bf):
        compressed = []
        crate = self._word
        word = []
        i = 0
        # iteratively find the compression rate
        while i < len(bf):
            plain = bf[i]
            if plain > self._climit:
                self._over[i] = plain
                plain = self._climit
            code = self._huff.encode([plain]) [0]   
            i += 1
            entries = len(word)
            if entries < crate:
                word.append(code)
                wordsize = sum(map(len,word))
                if wordsize > self._word:
                    compressed = []
                    word = []
                    crate = entries
                    i = 0
            else:
                compressed.append(word)
                word = [code]
        compressed.append(word)
        self._crate = crate
        return compressed

    def _compresshard (self,bf):
        compressed = []
        word = []
        i = 0
        # compress self._crate codes and put overflown to overflow list
        for i in bf:
            code = []
            if i > self._climit:
                self._over[i] = plain
                plain = self._climit
            code = self._huff.encode([plain]) [0]
            if len(word)+1 >= self._crate:
                compressed.append(word)
                word = [code]
            else: word.append(code)
        compressed.append(word)
        return compressed

    def _compressmax (self,bf):
        compressed = []
        word = []
        i = 0
        # compress as many codes as possible in every word. requires some indexing later
        while i < len(bf):
            plain = bf[i]
            if plain > self._climit:
                self._over[i] = plain
                plain = self._climit
            code = self._huff.encode([plain]) [0]
            i += 1
            wordsize = sum(map(len,word))
            if wordsize + len(code) < self._word:
                word.append(code)
            else:
                compressed.append(word)
                word = [code]         
        compressed.append(word)
        return compressed
    
    def size (self):
        # calc unpacked size
        #maxc = self.maxcounter()
        #usize = mul(self._m, add(int(floor(log(maxc,2))),1))
        numuwords = ceil((self._m * 3) / float(self._word))
        usize = numuwords * self._word
        # calc packed size
        numpwords = int(ceil(self._m / self._pack))
        usizep = self._word * numpwords
        # calc compressed size
        csize = mul(len(self._bf),self._word)
        # calc alternative compressed size
        #hccbf2 = self._compress2(reduce(add,self._bf))
        #numc2words = len(hccbf2) + ceil((log(self._m,2)) * len(hccbf2))/float(self._wordsize)
        #csize2 = numc2words * self._wordsize
        return usize,usizep,csize, 1 - csize/float(usize)
        #return usize,usizep,csize,csize2

    def distribution (self):
        dist = {}
        ind = 0
        for i in reduce(add,self._bf):
            c = self._huff.decode([i])[0]
            if c == self._climit:
                try: c = self._over[ind]
                except (KeyError): pass
            try: dist[c] += 1
            except (KeyError): dist[c] = 1
            ind += 1
        return dist

    def codedist (self):
        wordfills = [sum(map(len,self._bf[i])) for i in xrange(len(self._bf))]
        coded = {}
        for i in wordfills:
            try: coded[i] += 1
            except (KeyError): coded[i] = 1
        return coded

    def maxcounter (self):
        de = self._huff.decode(reduce(add,self._bf))
        #return max(de)
        if self._over.values()==[]:
            return max(de)
        return max([max(de),max(self._over.values())])

    def wordstat (self):
        wordfills = [sum(map(len,self._bf[i])) for i in xrange(len(self._bf))]
        numwords = len(self._bf)
        avgfill = sum(wordfills) / float(numwords)
        return min(wordfills),max(wordfills),avgfill

##############################################################
# MHT related functions
##############################################################
def mmht (c1,c2,d,n):
    m = []
    for i in x(1,d+1):
        m.append(c1 * pow(c2,(i-1)) * n)
    return m

def dmht (n): return int( ceil(log(log(n,2),2)) + 1 )

def p_crisis (c1,c2,n): return pow(n,-c1*c2)

class multihashtable ():
    def __init__ (self,n,c1,c2):
        self._n = n
        self._d = dmht(n)
        self._c = [c1,c2]
        self._m = mmht(c1,c2,d,n)

    def p_crisis (self):
        return pow(self._n,mul(-self._c[0],self._c[1]))

##############################################################
# MHT summary related
##############################################################
def msf (n): return mul(n,int(log(n,2)))

def msf2 (n): return int(pow(2,ceil(log(msf(n),2))))

def ksf (n): return int(floor(log(n,2)))

class singlefilter ():
    def __init__(self,n,mfn):
        self._n = n
        self._m = mfn(n)
        self._k = int(ceil(log(n,2)))

    def p_fp(self):
        return pow(1-pow(1-self._k/float(self._n),self._n),self._k)

    

##############################################################
# Huffman Class
##############################################################
class huffman ():
    class node:
        def __init__ (self,s,f,l = None, r = None):
            self._symbol = s
            self._freq = f
            self._left = l
            self._right = r
            
        def __cmp__ (self, other):
            return cmp(self._freq, other._freq)

        def __repr__ (self):
            return self._tostring(self,0,"")

        def __str__ (self):
            return self._tostring(self,0,"")

        def __getitem__(self,index):
            node,index = self._getitem(index)
            if not node: raise IndexError
            return node

        def _tostring(self,leaf,depth,b):
            if not leaf: return ""
            s = ""
            for i in xrange(depth):
                if b=="0": s += "|<<"
                elif b=="1": s += "|>>"
            s += "(%s,%s):%s\n" % (leaf._symbol, leaf._freq,b)
            s += self._tostring(leaf._left, depth+1, "0")
            s += self._tostring(leaf._right, depth+1, "1")
            return s

        def _getitem(self,index):
            if index == 0: return self,index
            node = None
            if self._left:
                node,index = self.left._getitem(index-1)
            if not node:
                if self._right:
                    node,index = self.right._getitem(index-1)
            return node,index

        class iterator(object):
            def __init__(self, root):
                self._root = root
                self._index = 0

            def __iter__(self):
                return self

            def next(self):
                try: item = self._root[self._index] 
                except IndexError: raise StopIteration 
                self.index += 1
                return item

        def __iter__(self): 
            return self.iterator(self)
        
    def __init__ (self,dist):
        self._root = self._buildtree(dist)
        self._codes = {}
        self._symbols = {}
        self._codebook(self._root,"")

    def __repr__ (self):
        return "%s,%s" % (repr(self._codes), repr(self._symbols))

    def _buildtree(self,dist):
        forest = sorted([self.node(s,dist[s]) for s in dist],reverse=True)
        while len(forest)>1:
            rtree = forest.pop()
            ltree = forest.pop()
            forest.append(self.node(None,ltree._freq+rtree._freq,ltree,rtree))
        return forest[0]

    def _codebook (self,node,code):
        if node._symbol != None:
            if code == "": code += "0"
            else:
                self._symbols[code] = node._symbol
                self._codes[node._symbol] = code
        else:
            if node._left: self._codebook(node._left,code+"0")
            if node._right: self._codebook(node._right,code+"1")

    def encode (self,symbols):
        codes = []
        for s in symbols:
            try: codes.append(self._codes[s])
            except (KeyError): print >> sys.stderr, "symbol %s not found" % (s)
        return codes

    def decode (self,codes):
        symbols = []
        #test = ""
        for c in codes:
            #test.join(c)
            try:
                symbols.append(self._symbols[c])
                #test = ""
            except (KeyError): print >> sys.stderr, "code %s not found" % (c)
        return symbols
    
##############################################################
# Hash Functions
##############################################################
class uhash ():
    def __init__ (self,k,bits, maxv):
        self._k = k
        self._bits = bits
        self._lut = [[random.randint(0,maxv-1) for i in xrange(self._bits)] for j in xrange(self._k)]

    def hash (self,key):
        h = [0] * self._k
        for b in xrange(self._bits):
            if (key>>b) & 1:
                for k in xrange(self._k):
                    h[k] ^= self._lut[k][b]
        return {}.fromkeys(h).keys()


##############################################################
# Fast Hash Table
##############################################################
class fasthashtable2 ():
    class entry ():
        def __init__ (self,k,v=None,h=None):
            self.k = k
            self.v = v
            self.h = h
        def __repr__ (self):
            return "%s:(%s,%s)" %(self.k, self.v, self.h)
        def __str__ (self):
            return "%s:(%s,%s)" %(self.k, self.v, self.h)
        def __hash__ (self):
            return hash(self.k)
        def __cmp__ (self,other):
            return cmp(hash(self),hash(other))
        
    def __init__ (self, n, c, mfn, k, width, word, climit):
        self._n  = n
        self._climit = climit
        self._c = c
        self._m = mfn(n,c)
        self._k = kbf(self._n,self._m) if k<=0 else k
        self._width = width
        self._word = word
        self._cbf = countingbloomfilter(self._n,self._c,mfn,self._k,kbf)
        self._hash = uhash(self._k,32,self._m)
        self._tbl = [[] for i in xrange(self._m)]
        self._pruned = False
        self._fp = 0
        self._putfn = self._putb
        self._popfn = self._popb
        self._getfn = self._getb

    def __repr__ (self):
        return "<type 'fasthashtable'>"

    def __str__ (self):
        return "%s,%s,%s" % (self._n,self._m,self._k)

    def _hashcounter (self,key):
        h = self._hash.hash(key)
        c = self._cbf.get(h)
        i = h[c.index(min(c))]
        return h,c,i

    def put (self,key,val):
        return self._putfn(key,val)

    def _putb (self,key,val):
        hashs,counter,mlb = self._hashcounter(key)
        try:
            i = self._tbl[mlb].index(key)
            self._tbl[mlb][i].v = val
            return hashs,counter,mlb
        except (ValueError): pass
        e = self.entry(key,val,mlb)
        for h in hashs:
            self._tbl[h].append(e)
            self._cbf.inc([h])
        return hashs,counter,mlb

    def _putp (self,key,val):
        hashs,counter,mlb = self._hashcounter(key)
        try:
            i = self._tbl[mlb].index(key)
            self._tbl[mlb][i].v = val
            return hashs,counter,mlb
        except (ValueError): pass
        load = sum(map(lambda x: 1 if x.h == mlb else 0,self._tbl[mlb]))
        if min(counter) >= self._climit or load >= self._width: mlb = self._m
        e = self.entry(key,val,mlb)
        reloc = []
        # insert into off
        for h in hashs:
            # get for relocation
            for t in self._tbl[h]:
                if t.h == h: reloc.insert(0,t)
                elif t.h == self._m: reloc.append(t)
            self._tbl[h].append(e)
            self._cbf.inc([h])
        # do relocation
        for r in reloc:
            hr,cr,ir = self._hashcounter(r.k)
            load = sum(map(lambda x: 1 if x.h == ir else 0,self._tbl[ir]))
            if min(cr) >= self._climit or load >= self._width: r.h = self._m
            else: r.h = ir
        return hashs,counter,mlb
      
    def get (self,key):
        return self._getfn(key)

    def _getb (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        val = None
        if min(counter) == 0: return val
        try:
            index = self._tbl[mlb].index(key)
            val = self._tbl[mlb][index].v
        except (ValueError): self._fp += 1
        return val

    def _getp (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        val = None
        if min(counter) == 0: return val
        try:
            index = self._tbl[mlb].index(key)
            e = self._tbl[mlb][index]
            if e.h == mlb or e.h == self._m:
                val = e.v
        except (ValueError): self._fp += 1
        return val

    def pop (self,key):
        return self._popfn(key)

    def _popb (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        if min(counter) == 0: return None
        fail = 0
        for h in hashs:
            try:
                self._tbl[h].remove(key)
                self._cbf.dec([h])
            except (ValueError): fail += 1
        return fail        

    def _popp (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        if min(counter) == 0: return None
        fail = 0
        reloc = []
        # delete from off
        for h in hashs:
            try:
                self._tbl[h].remove(key)
                self._cbf.dec([h])
                reloc += self._tbl[h]
            except (ValueError): fail += 1
        # relocate
        for r in reloc:
            hr,cr,ir = self._hashcounter(r.k)
            load = sum(map(lambda x: 1 if x.h == ir else 0,self._tbl[ir]))
            if min(cr) >= self._climit or load >= self._width: ir = self._m
            if ir != r.h: r.h = ir
        return fail

    def prune (self):
        # compress bloom filter
        self._cbf = huffmancbf(self._cbf,self._word,self._climit)
        # do the pruning
        for i in xrange(self._m):
            bucket = self._tbl[i]
            for e in bucket:
                hashs,counter,mlb = self._hashcounter(e.k)
                load = sum(map(lambda x: 1 if x.h == mlb else 0,self._tbl[mlb]))
                if min(counter) >= self._climit or load >= self._width: e.h = self._m
                else: e.h = mlb
        # set function pointers
        self._putfn = self._putp
        self._popfn = self._popp
        self._getfn = self._getp
        self._pruned = True
        return True

    def distribution (self):
        cbfd = self._cbf.distribution()
        onld = {}
        camd = []
        coded = self._cbf.codedist()
        for i in range(len(self._tbl)):
            bucket = self._tbl[i]
            nume = 0
            for e in bucket:
                if e.h == i: nume += 1
                elif e.h == self._m:
                    try: camd.index(e)
                    except (ValueError): camd.append(e)
            try: onld[nume] += 1
            except (KeyError): onld[nume] = 1
        return cbfd,onld,len(camd)


class fasthashtable ():
    class entry ():
        def __init__ (self,k,v=None,h=None):
            self.k = k
            self.v = v
            self.h = h
        def __repr__ (self):
            return "%s:(%s,%s)" %(self.k, self.v, self.h)
        def __str__ (self):
            return "%s:(%s,%s)" %(self.k, self.v, self.h)
        def __hash__ (self):
            return hash(self.k)
        def __cmp__ (self,other):
            return cmp(hash(self),hash(other))
        
    def __init__ (self, n, c, mfn, k, width, wordsize, maxc):
        self._n  = n
        #self._shifts = shifts
        self._maxc = maxc
        self._c = c
        self._m = mfn(n,c)# >> shifts
        self._k = kbf(self._n,self._m) if k<=0 else k
        self._width = width
        self._word = wordsize
        #self._cbf = countingbloomfilter(self._n,self._c/2**self._shifts,mfn,self._k,kbf)
        self._cbf = countingbloomfilter(self._n,self._c,mfn,self._k,kbf)
        self._hash = uhash(self._k,32,self._m)
        self._off = [[] for i in xrange(self._m)]
        self._pruned = False
        self._fp = 0
        self._putfn = self._putb
        self._popfn = self._popb
        self._getfn = self._getb

    def __repr__ (self):
        return "<type 'fasthashtable'>"

    def __str__ (self):
        return "%s,%s,%s" % (self._n,self._m,self._k)

    def _hashcounter (self,key):
        h = self._hash.hash(key)
        c = self._cbf.get(h)
        i = h[c.index(min(c))]
        return h,c,i

    def put (self,key,val):
        return self._putfn(key,val)

    def _putb (self,key,val):
        hashs,counter,mlb = self._hashcounter(key)
        try:
            i = self._off[mlb].index(key)
            self._off[mlb][i].v = val
            return hashs,counter,mlb
        except (ValueError): pass
        e = self.entry(key,val,mlb)
        for h in hashs:
            self._off[h].append(e)
            self._cbf.inc([h])
        return hashs,counter,mlb

    def _putp (self,key,val):
        hashs,counter,mlb = self._hashcounter(key)
        try:
            i = self._off[mlb].index(key)
            self._off[mlb][i].v = val
            return hashs,counter,mlb
        except (ValueError): pass
        e = self.entry(key,val,mlb)
        reloc = []
        # insert into off
        for h in hashs:
            self._off[h] += [e]
            self._cbf.inc([h])
            # get onl entries for relocation
            reloc += self._onl[h]
            self._onl[h] = []
            # get cam reloc entries
            try: reloc += self._cam.pop(h)
            except (KeyError): pass
        # do relocation
        for r in reloc:
            hr,cr,ir = self._hashcounter(r.k)
            r.h = ir          
            self._putonlcam(r)
        # insert into onl
        self._putonlcam(e)
        return hashs,counter,mlb

    def _putonlcam (self,e):
        entries = self._onl[e.h]
        if len(entries)< self._width: entries.append(e)
        else:
            try: self._cam[e.h].append(e)
            except (KeyError): self._cam[e.h] = [e]
            #try: self._cam.index(e)
            #except (ValueError): self._cam.append(e) 
        return True

    def _putcam (self,e):
        try: self._cam[e.h].append(e)
        except (KeyError): self._cam[e.h] = [e]
        return True
        
    def get (self,key):
        return self._getfn(key)

    def _getb (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        val = None
        if min(counter) == 0: return val
        try:
            index = self._off[mlb].index(key)
            val = self._off[mlb][index].v
        except (ValueError): self._fp += 1
        return val

    def _getp (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        val = None
        if min(counter) == 0: return val
        try:
            index = self._onl[mlb].index(key)
            val = self._onl[mlb][index].v
        except (ValueError):
            try:
                index = self._cam[mlb].index(key)
                val = self._cam[mlb][index].v
            except (KeyError,ValueError): self._fp += 1
        return val

    def pop (self,key):
        return self._popfn(key)

    def _popb (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        if min(counter) == 0: return None
        fail = 0
        for h in hashs:
            try:
                self._off[h].remove(key)
                self._cbf.dec([h])
            except (ValueError): fail += 1
        return fail        

    def _popp (self,key):
        hashs,counter,mlb = self._hashcounter(key)
        if min(counter) == 0: return None
        fail = 0
        reloc = []
        # delete from off
        for h in hashs:
            try:
                self._off[h].remove(key)
                self._cbf.dec([h])
                reloc += self._off[h]
            except (ValueError): fail += 1
        # delete from onl / cam
        self._poponlcam(key,mlb)
        # relocate
        for r in reloc:
            hr,cr,ir = self._hashcounter(r.k)
            if ir != r.h:
                self._poponlcam(r.k,r.h)
                r.h = ir
                self._putonlcam(r)
        return fail

    def _poponlcam (self,key,mlb):
        try: self._onl[mlb].remove(key)
        except (ValueError):
            try: self._cam.pop(key)
            except (ValueError,KeyError): return False
        return True

    def prune (self):
        # prepare onl data structures
        self._onl = [[] for i in xrange(self._m)]
        self._cam = {}
        #self._cam = []
        # do the pruning
        for i in xrange(self._m):
            bucket = self._off[i]
            for e in bucket:
                hashs,counter,mlb = self._hashcounter(e.k)
                e.h = mlb
                if mlb == i:
                    if min(counter)==self._maxc: self._putcam(e)
                    else :self._putonlcam(e)
        # compress bloom filter
        self._cbf = huffmancbf(self._cbf,self._word,self._maxc)
        # set function pointers
        self._putfn = self._putp
        self._popfn = self._popp
        self._getfn = self._getp
        self._pruned = True
        return True

    def distribution (self):
        cbfd = self._cbf.distribution()
        onld = {}
        for i in self._onl:
            key = len(i)
            try: onld[key] += 1
            except (KeyError): onld[key] = 1
        return cbfd,onld,len(self._cam)

##############################################################
# MAIN
##############################################################
class config ():
    def __init__ (self,n=100,climit=4,c=3.2,k=0,width=4,word=32,cdist=10,sims=1):
        self.n = n
        self.climit = climit
        self.c = c
        self.k = k
        self.width = width
        self.word = word
        self.sims = sims
        self.cdist = cdist
        
def checkargv (argv):
    cfg = config()
    try: opts,args = getopt.getopt(argv,"n:m:c:k:w:s:r:h", ["help"])
    except (getopt.GetoptError): usage()
    for opt, arg in opts:
        if opt == "-n": cfg.n = int(arg)
        elif opt == "-m": cfg.climit = int(arg)
        elif opt == "-c": cfg.c = float(arg)
        elif opt == "-k": cfg.k = int(arg)
        elif opt == "-w": cfg.width = int(arg)
        elif opt == "-s": cfg.sims = int(arg)
        elif opt == "-r": cfg.word = int(arg)
        elif opt == "-d": cfg.cdist = int(arg)
        elif opt in ("-h","--help"): usage()
    return cfg

def usage():
    print "usage: python iplookup.py [options]"
    print
    print "\t-n: number of items"
    print "\t-m: maximum online counter value allowed"
    print "\t-c: multiplier constant for size computation"
    print "\t-k: number of hash functions"
    print "\t-w: bucket width for onl table"
    print "\t-r: sram wordsize in bits"
    print "\t-d: maximum counter range to include in distribution statistics"
    print "\t-s: number of simulation runs"
    sys.exit(2)

class fhtsimulator ():
    def __init__ (self,cfg):
        self.cfg = cfg

    def run(self):
        # build header
        header = "n,c,m,k,climit,width,word,crate,usize,psize,csize,cfactor,minfill,maxfill,avgfill,maxc,fp"
        lowestcrate = self.cfg.word / self.cfg.climit
        for i in xrange(lowestcrate,self.cfg.word+1): header += ",b%s" % i
        for i in xrange(self.cfg.cdist): header += ",c%s" % (i)
        for i in xrange(self.cfg.width + 1): header += ",t%s" % (i)
        header += ",camsize"
        print header
        # do the sims
        for i in xrange(self.cfg.sims):
            print >> sys.stderr, "run: %s / %s" % (i+1,self.cfg.sims)
            #self.fht = fasthashtable(self.cfg.n,self.cfg.m,self.cfg.c,mfht,self.cfg.k,self.cfg.w,self.cfg.wordsize)
            self.fht = fasthashtable2(self.cfg.n,self.cfg.c,mfht,self.cfg.k,self.cfg.width,self.cfg.word,self.cfg.climit)
            self.items = {}
            start = time.time()
            self.put(self.cfg.n)
            self.prune()
            self.get()
            print self.statistics()
            stop = time.time() - start
            print >> sys.stderr, "runtime: %s" % stop
            
    def prune (self):
        start = time.time()
        self.fht.prune()
        stop = time.time() - start

    def put (self,n):
        start = time.time()
        for i in xrange(n):
            key = random.randint(0,int(2**32))
            val = random.randint(0,int(2**16))
            self.fht.put(key,val)
            self.items[key] = val
        stop = time.time() - start
        #print >> sys.stderr, "put %s" % (stop)

    def get (self):
        start = time.time()
        for key in self.items.keys():
            val = self.items[key]
            ret = self.fht.get(key)
            if ret != val: print >> sys.stderr, "could not retrieve: %s:%s" %(key,val)
        stop = time.time() - start
        #print >> sys.stderr, "get %s" %(stop)

    def pop (self,n):
        start = time.time()
        keys = self.items.keys()[:]
        for i in xrange(n):
            key = keys[i]
            self.fht.pop(key)
            self.items.pop(key)
        stop = time.time() - start
        #print >> sys.stderr, "pop %s" %(stop)

    def statistics (self):
        # header:
        # n,c,m,k,climit,width,word,crate,usize,psize,csize,cfactor,minfill,maxfill,avgfill,maxc,fp
        # codedist,cdist,onldist
        stats = "%s,%s,%s,%s,%s,%s" % (self.fht._n, self.cfg.c, self.fht._m, self.fht._k, self.fht._climit, self.fht._width)
        # compression stuff
        if self.fht._pruned:
            stats += ",%s,%s" % (self.fht._cbf._word,self.fht._cbf._crate)
            stats += ",%s,%s,%s,%s" % (self.fht._cbf.size())
            stats += ",%s,%s,%s" % (self.fht._cbf.wordstat())
        else:
            stats += ",0,0,%s,0,0,0,0,0,0" % (self.fht._cbf.size())
        stats += ",%s,%s" % (self.fht._cbf.maxcounter(),self.fht._cbf.p_fp())
        # calculate word fill distribution
        codedist = self.fht._cbf.codedist()
        lowestcrate = self.cfg.word / self.cfg.climit
        for i in xrange(lowestcrate,self.cfg.word+1):
            try: stats += ",%s" % (codedist[i])
            except (KeyError): stats += ",0"
        # calculate counter and online distribution
        cbfd,onld,camsize = self.fht.distribution()
        for i in xrange(self.cfg.cdist):
            try: stats += ",%s" % (cbfd[i])
            except (KeyError): stats += ",0"
        for i in xrange(self.cfg.width+1):
            try: stats += ",%s" % (onld[i])
            except (KeyError): stats += ",0"
        stats += ",%s" % camsize
        return stats

def main (argv):
    cfg = checkargv(argv)
    sim = fhtsimulator(cfg)
    sim.run()             
    return sim

if __name__=="__main__":
    sim = main(sys.argv[1:])
          
